# Réalisé par Chebbi Achref MPISRCC2

---

# Pizza CI/CD Project
Ce projet présente un pipeline CI/CD  pour un site web Dockerisé utilisant GitLab CI/CD. Le pipeline se compose de quatre étapes : <br> Login,  Build, Push, et Logout. Il est conçu pour  build une image Docker, push sur DockerHub, et effectuer les opérations de login/logout nécessaires.
<br>
<img align="center" src="gitlabimage/gitlabpip.png" alt="build" height="350" width="750" />
<br>
Runner 
<br>
<img align="center" src="gitlabimage/runner.png" alt="build" height="350" width="750" />

 
## Stages

### 1. Login
   - Se connecte à DockerHub en utilisant les identifiants fournis.

### 2. Build
 <img align="center" src="gitlabimage/image.png" alt="build" height="250" width="450" />
 <br>
   - Builds une image Docker :  docker build -t $CONTAINER_TEST_IMAGE .


### 3. Push
 <img align="center" src="gitlabimage/dockerhub.png" alt="push" height="450" width="750" /> 
 <br>
   - Tags l'image Docker avec  `$CI_COMMIT_REF_NAME` et pushes it to DockerHub.
   <br>
   - Tags la même image comme latest et pushes sur DockerHub.

### 4. Logout
   - Se déconnecte de DockerHub pour assurer un contrôle d'accès sécurisé.

## Variables

- `CONTAINER_TEST_IMAGE`: Le nom de l'image Docker à built et pushed.
- `DOCKERHUB_USERNAME`: DockerHub username pour  l'authentication.

## Usage

1. Fork/Clonez le dépôt.
2. Configurez les variables CI/CD de GitLab (DOCKERHUB_USERNAME, etc.) dans les paramètres de votre projet.
3. Personnalisez le Dockerfile et d'autres configurations selon vos besoins.
4. Effectuez un commit et poussez vos modifications pour déclencher le pipeline CI/CD.

